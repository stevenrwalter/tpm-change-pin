use pkcs11::types::{CKF_RW_SESSION, CKF_SERIAL_SESSION};
use rpassword::prompt_password;

fn main() {
    let old_pin = prompt_password("Enter the old PIN: ").unwrap();
    let pin1 = prompt_password("Enter the new PIN: ").unwrap();
    let pin2 = prompt_password("Confirm the new PIN: ").unwrap();

    if pin1 != pin2 {
        eprintln!("The PINs do not match.");
        return;
    }

    if pin1.len() < 6 {
        eprintln!("PINs must be a least 6 characters long.");
        return;
    }

    if pin1 == "123456" {
        eprintln!("Think of a better PIN.");
        return;
    }

    let mut any_different = false;
    let first_char = pin1.chars().next().unwrap();
    for c in pin1.chars().take(4) {
        if c != first_char {
            any_different = true;
            break;
        }
    }

    if !any_different {
        eprintln!("Think of a better PIN.");
        return;
    }

    let ctx = pkcs11::Ctx::new_and_initialize("/usr/lib/x86_64-linux-gnu/pkcs11/libtpm2_pkcs11.so")
        .expect("initialize");
    let slots = ctx.get_slot_list(true).expect("slots");
    let session = ctx
        .open_session(slots[0], CKF_SERIAL_SESSION | CKF_RW_SESSION, None, None)
        .expect("session");
    ctx.set_pin(session, Some(&old_pin), Some(&pin1))
        .expect("set pin");
}
