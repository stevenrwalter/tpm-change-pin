prefix ?= /usr
bindir ?= $(prefix)/bin

all:
	cargo build --release

install:
	mkdir -p $(DESTDIR)$(bindir)
	install -m755 -t $(DESTDIR)$(bindir) target/release/tpm-change-pin
